# WIP - Things are broken


# Setup

- Install Helm (https://docs.helm.sh/using_helm/#installing-helm)
- `helm init`
- `helm repo update`
- `helm install --wait .` (`--wait` is important)
- Head to `https://minds.local/` (initial username: `minds` password: `Pa$$w0rd`)

# Manual setup

- `helm template -n release . > release.yaml`
- `kubectl create -f release.yaml`
- `helm template -x templates/job-installer.yaml -n release . > release-installer.yaml`

# Manual cleanup

- `kubectl delete ingress,statefulset,configmaps,service,pods,deployments,jobs,pvc,roles,rolebindings,secrets --all`

# Improving working directory mount performance

```
docker run -v /Users/Mark/Documents/Minds/:/var/www/Minds:delegated alpine sleep 1d
```

# Creating encryption keys

```
openssl genrsa -des3 -out ./certs/private.pem 4096
openssl rsa -in ./certs/private.pem -outform PEM -pubout -out ./certs/public.pem
```

# Updating dependencies

`helm dependency update`

# TODO

- Implement MongoDB and get Boost working
- Autoconfigure ES mappings
- Improve speed when mounting local directories
- Launch to AWS
- Implement socket server
- Integrate with CI flow and autolaunch